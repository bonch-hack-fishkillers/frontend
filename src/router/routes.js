
const routes = [
  {
    path: '/',
    component: () => import('layouts/Clear.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'find', component: () => import('pages/find.vue') }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/Main.vue'),
    children: [
      { path: 'shelters', component: () => import('pages/Shelters.vue') },
      { path: 'pets', component: () => import('pages/pets.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
