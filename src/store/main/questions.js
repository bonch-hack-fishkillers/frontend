import API from '../../boot/index'

const state = {
  current: 0,
  questions: null
}
const getters = {
  getQuestions (state) {
    return state.questions
  },
  getCurrent (state) {
    return state.current
  }
}
const actions = {
  changeCurrent (context, payload) {
    context.commit('CHANGE_CURRENT', payload)
  },
  getQuestions (context, data) {
    return new Promise((resolve, reject) => {
      API.get('questions')
        .then(response => {
          context.commit('GET_QUESTIONS', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}
const mutations = {
  CHANGE_CURRENT (state, payload) {
    state.current = payload
  },
  GET_QUESTIONS (state, data) {
    state.questions = data
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
