import Vue from 'vue'
import Vuex from 'vuex'
import Main from './main'

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      Main
    },
    strict: process.env.DEV
  })

  return Store
}
