import axios from 'axios'

export const baseURL = 'localhost:8080/'
export const API_URL = baseURL + 'api/'

const API = axios.create({
  baseURL: API_URL
})

export default API
