import questions from './questions'

export default {
  namespaced: true,
  modules: {
    questions
  }
}
